import 'package:flutter/material.dart';

import 'package:provider_day4/drawer_menu.dart';
import 'package:provider/provider.dart';
import 'package:provider_day4/model/ui.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About'),
        backgroundColor: Colors.teal,
      ),
      drawer: DrawerMenu(),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: Consumer<UI>(
          builder: (context, ui, child) {
            return Center(
              child: Text(
                'Welcome Dung, this is about page, font size: ${ui.fontSize.toInt()}',
                style:
                    TextStyle(fontSize: ui.fontSize, color: Colors.lightBlue),
              ),
            );
          },
        ),
      ),
    );
  }
}
