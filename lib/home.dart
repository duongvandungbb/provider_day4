import 'package:flutter/material.dart';
import 'package:provider_day4/drawer_menu.dart';

import 'package:provider/provider.dart';
import 'package:provider_day4/model/ui.dart';

const kAppTitle = 'Home';
const kStateType = 'Provider';

class Home extends StatelessWidget {
  // String text = 'This is home page !';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(kAppTitle),
        backgroundColor: Colors.teal,
      ),
      drawer: DrawerMenu(),
      body: Container(
        margin: EdgeInsets.all(10),
        child: Consumer<UI>(
          builder: (context, ui, child) {
            return Center(
              child: Text(
                'Welcome Dung, this is home page, font size: ${ui.fontSize.toInt()}',
                style: TextStyle(fontSize: ui.fontSize, color: Colors.black),
              ),
            );
          },
        ),
      ),
    );
  }
}
